const express = require('express');
const authRote = require('./routes/auth-routes')

const app = express();

//set up view engine
app.set('view engine', 'ejs')

//set up route
app.use('./auth', authRote);

//create home route
app.get('/', (req, res) => {
    res.render('home');
});

app.listen(3000, () => {
    console.log('app now listening for request on port 3000');
});