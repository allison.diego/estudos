const router = require('express').Router();

// auth login
router.get('/login', (req, res) => {
    res.render('login');
});

// auth logout 
router.get('logout', (req, res) => {
    //handle with passport
    res.send('logging out')
})

//auth with strava
router.get('/strava', (req, res) => {
    // handle with passport
    res.send('loggin in with strava');
});

module.exports = router;