import 'package:aula1/drawer_list.dart';
import 'package:aula1/pages/hello_listview.dart';
import 'package:aula1/pages/hello_page2.dart';
import 'package:aula1/pages/hello_page3.dart';
import 'package:aula1/utils/nav.dart';
import 'package:aula1/widgets/blue_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                text: "Tab 1 ",
              ),
              Tab(
                text: "Tab 2 ",
              ),
              Tab(
                text: "Tab 3 ",
              )
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            _body(context),
            Container(
              color: Colors.green,
            ),
            Container(
              color: Colors.yellow,
            ),
          ],
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () => _onClickFab(),
            ),
          ],
        ),
        drawer: DrawerList(),
      ),
    );
  }

  _onClickFab() {
    print("Adicionar");
  }

  _body(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _text(),
          _images(),
          _buttons(),
        ],
      ),
    );
  }

  _images() {
    return Container(
      height: 300,
      margin: EdgeInsets.only(
        left: 10,
        top: 20,
        right: 10,
        bottom: 20,
      ),
      child: PageView(
        children: <Widget>[
          _img("assets/images/dog1.png"),
          _img("assets/images/dog2.png"),
          _img("assets/images/dog3.png"),
          _img("assets/images/dog4.png"),
          _img("assets/images/dog5.png"),
        ],
      ),
    );
  }

  _buttons() {
    return Builder(
      builder: (context) {
        return Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                BlueButtton(
                  "ListView",
                  onPressed: () => _onClickNavigator(context, HelloListView()),
                ),
                BlueButtton(
                  "Page 2",
                  onPressed: () => _onClickNavigator(context, HelloPage2()),
                ),
                BlueButtton(
                  "Page 3",
                  onPressed: () => _onClickNavigator(context, HelloPage3()),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                BlueButtton("Snack", onPressed: () => _onClickSnack(context)),
                BlueButtton("Dialog", onPressed: () => _onClickDialog(context)),
                BlueButtton("Toast", onPressed: () => _onClickToast(context)),
              ],
            ),
          ],
        );
      },
    );
  }

  _text() {
    return Text(
      "Hello World",
      style: TextStyle(
        color: Colors.green,
        fontSize: 20,
        fontWeight: FontWeight.bold,
        fontStyle: FontStyle.italic,
        decoration: TextDecoration.underline,
        decorationColor: Colors.green,
        decorationStyle: TextDecorationStyle.dotted,
      ),
    );
  }

  _onClickNavigator(context, page) async {
    String s = await push(context, page);
    print(">> $s");
  }

  _onClickToast(context) {
    Fluttertoast.showToast(
        msg: "Flutter é muito legal",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.black45,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  _onClickDialog(context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              title: Text("Flutter é muito legal"),
              actions: <Widget>[
                FlatButton(
                  child: Text("Cancelar"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  child: Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context);
                    print("Ok!!!");
                  },
                )
              ],
            ),
          );
        });
  }

  _onClickSnack(context) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text("Olá Flutter"),
      action: SnackBarAction(
        textColor: Colors.yellow,
        label: "Ok",
        onPressed: () => print("Ok"),
      ),
    ));
  }

  _img(String path) {
    return Image.asset(
      path,
      fit: BoxFit.cover,
    );
  }
}

class _onClickFab {}
