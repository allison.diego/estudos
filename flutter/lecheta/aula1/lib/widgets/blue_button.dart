import 'package:flutter/material.dart';

class BlueButtton extends StatelessWidget {
  String text;
  Function onPressed;

  BlueButtton(this.text, {@required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.blue,
      child: Text(
        text,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
      ),
      onPressed: () => onPressed(),
    );
  }
}
