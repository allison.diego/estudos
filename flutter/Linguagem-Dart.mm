<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1567733162353" ID="ID_467271408" MODIFIED="1567733691526" STYLE="fork" TEXT="Linguagem Dart">
<node CREATED="1567733185436" FOLDED="true" ID="ID_1668130468" MODIFIED="1567734130590" POSITION="right" TEXT="Comet&#xe1;rios">
<node CREATED="1567733219847" FOLDED="true" ID="ID_983593223" MODIFIED="1567733627717" TEXT="Linha">
<node CREATED="1567733228670" ID="ID_1084521529" MODIFIED="1567733234668" TEXT="//"/>
</node>
<node CREATED="1567733235813" FOLDED="true" ID="ID_1479665078" MODIFIED="1567733611951" TEXT="Bloco">
<node CREATED="1567733245716" ID="ID_418941914" MODIFIED="1567733278145" TEXT="Inicio">
<node CREATED="1567733278148" ID="ID_361749436" MODIFIED="1567733282294" TEXT="/*"/>
</node>
<node CREATED="1567733283086" ID="ID_197686905" MODIFIED="1567733293215" TEXT="Fim">
<node CREATED="1567733293774" ID="ID_1558167139" MODIFIED="1567733295482" TEXT="*/"/>
</node>
</node>
<node CREATED="1567733301479" FOLDED="true" ID="ID_1166687393" MODIFIED="1567733638877" TEXT="Documenta&#xe7;&#xe3;o">
<node CREATED="1567733463726" FOLDED="true" ID="ID_1297228207" MODIFIED="1567733637357" TEXT="Linha">
<node CREATED="1567733314627" ID="ID_1108151171" MODIFIED="1567733320000" TEXT="///"/>
</node>
<node CREATED="1567733444541" FOLDED="true" ID="ID_927673562" MODIFIED="1567733636310" TEXT="Bloco">
<node CREATED="1567733498425" FOLDED="true" ID="ID_882485864" MODIFIED="1567733635222" TEXT="Inicio">
<node CREATED="1567733480901" ID="ID_386088891" MODIFIED="1567733487234" TEXT="/**"/>
</node>
<node CREATED="1567733491630" FOLDED="true" ID="ID_269478884" MODIFIED="1567733635677" TEXT="Fim">
<node CREATED="1567733488118" ID="ID_1576608348" MODIFIED="1567733489805" TEXT="*/"/>
</node>
</node>
</node>
</node>
<node CREATED="1567733647626" ID="ID_1017614965" MODIFIED="1567733686317" POSITION="right" TEXT="Conceitos">
<node CREATED="1567733693406" ID="ID_1829454258" MODIFIED="1567733846400" TEXT="Tudo que se pode colocar em uma vari&#xe1;vel &#xe9; um objeto. E todo objeto &#xe9; uma inst&#xe2;ncia de uma classe."/>
<node CREATED="1567733848557" ID="ID_1347011289" MODIFIED="1567734009247" TEXT="Dart &#xe9; uma linguagem fortemente tipada. Mas quando quisermos, podemos explicitar que nenhum tipo &#xe9; esperando utilizando o tipo especial &apos;dynamic&apos;."/>
<node CREATED="1567734010569" ID="ID_1664659462" MODIFIED="1567734065957" TEXT="Dart suporta tipos gen&#xe9;ricos, como uma lista de inteiros (List&lt;int&gt;)"/>
<node CREATED="1567734068245" ID="ID_1535125465" MODIFIED="1567734126270" TEXT="O Dart suporta fun&#xe7;&#xf5;es de n&#xed;vel superior (como main()), bem como fun&#xe7;&#xf5;es vinculadas a uma classe ou objeto (m&#xe9;todos est&#xe1;tico e de inst&#xe2;ncia, respectivamente). Voc&#xea; tamb&#xe9;m pode criar fun&#xe7;&#xf5;es dentro de fun&#xe7;&#xf5;es (fun&#xe7;&#xf5;es aninhadas ou locais)."/>
<node CREATED="1567734128076" ID="ID_975800403" MODIFIED="1567734193495" TEXT="Da mesma forma, o Dart suporta vari&#xe1;veis de n&#xed;vel superior, bem como vari&#xe1;veis vinculadas a uma classe ou objeto (vari&#xe1;veis est&#xe1;ticas e de inst&#xe2;ncia). As vari&#xe1;veis de inst&#xe2;ncia s&#xe3;o algumas vezes conhecidas como &apos;fields&apos; ou  &apos;properties&apos;."/>
<node CREATED="1567734216024" ID="ID_1787212128" MODIFIED="1567734537760" TEXT="Dart n&#xe3;o suporta as palavras-chave &apos;public&apos;, &apos;protected&apos; e &apos;private&apos;. Se um identificador iniciar com underline &apos;_&apos; significa que este &#xe9; privado para a biblioteca em quest&#xe3;o"/>
<node CREATED="1567734493417" ID="ID_1347458751" MODIFIED="1567734548192" TEXT="Identifcadore s&#xf3; poder&#xe3;o come&#xe7;ar com uma letra ou underline &apos;_&apos;"/>
<node CREATED="1567734660516" FOLDED="true" ID="ID_1518912497" MODIFIED="1567734833287" TEXT="As ferramentas Dart podem relatar dois tipos de problemas">
<node CREATED="1567734675055" FOLDED="true" ID="ID_1992052042" MODIFIED="1567734829085" TEXT="Avisos">
<node CREATED="1567734680672" ID="ID_943361137" MODIFIED="1567734697644" TEXT="Os avisos s&#xe3;o apenas indica&#xe7;&#xf5;es de que seu c&#xf3;digo pode n&#xe3;o funcionar, mas eles n&#xe3;o impedem a execu&#xe7;&#xe3;o do seu programa"/>
</node>
<node CREATED="1567734700135" FOLDED="true" ID="ID_1286297977" MODIFIED="1567734826441" TEXT="Erros">
<node CREATED="1567734751498" FOLDED="true" ID="ID_307519693" MODIFIED="1567734823918" TEXT="De tempo de compila&#xe7;&#xe3;o">
<node CREATED="1567734766378" ID="ID_529655790" MODIFIED="1567734785692" TEXT=" Um erro em tempo de compila&#xe7;&#xe3;o impede a execu&#xe7;&#xe3;o do c&#xf3;digo" VSHIFT="1"/>
</node>
<node CREATED="1567734787140" FOLDED="true" ID="ID_776197377" MODIFIED="1567734825255" TEXT="De tempo de execu&#xe7;&#xe0;o">
<node CREATED="1567734799650" ID="ID_1615387542" MODIFIED="1567734808139" TEXT="Um erro em tempo de execu&#xe7;&#xe3;o resulta em uma exce&#xe7;&#xe3;o sendo gerada enquanto o c&#xf3;digo &#xe9; executado"/>
</node>
</node>
</node>
</node>
</node>
</map>
